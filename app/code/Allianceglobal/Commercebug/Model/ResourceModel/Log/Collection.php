<?php
/**
* Copyright © Pulse Storm LLC 2016
* All rights reserved
*/
namespace Allianceglobal\Commercebug\Model\ResourceModel\Log;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Allianceglobal\Commercebug\Model\Log','Allianceglobal\Commercebug\Model\ResourceModel\Log');
    }
}
