<?php
/**
* Copyright © Pulse Storm LLC 2016
* All rights reserved
*/
namespace Allianceglobal\Commercebug\Observers;
class Model extends AbstractObserver
{
    protected function _execute(\Magento\Framework\Event\Observer $observer)
    {
        return $this->getModelInformation($observer);
    }
    
    public function getModelInformation($observer)
    {
        \Allianceglobal\Commercebug\Model\All::addTo('models', $observer->getObject());
    }
}